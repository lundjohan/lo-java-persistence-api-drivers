package se.miun.dsv.jee.jpa.example;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import se.miun.dsv.jee.jpa.example.model.Watermelon;

/**
 * The class that populates the database. When you have your database setup,
 * running the main method should create and persist 100 watermelons with their
 * seeds using the "WATERMELON" join column in the SEEDS database table.
 *
 */
public class DatabasePopulator {

	private static final int NUMBER_MELONS = 100;

	private static final int MAX_NUMBER_SEEDS = 100;

	public static void main(String[] args) {
		Set<Watermelon> watermelons = createRandomMelons();
		MyWaterMelonDAO dao = new MyWaterMelonDAO();
		dao.persistAll(watermelons);
		dao.shutDown();
	}

	private static Set<Watermelon> createRandomMelons() {
		WaterMelonFactory factory = new WaterMelonFactory(MAX_NUMBER_SEEDS);
		Set<Watermelon> watermelons = new HashSet<>();
		IntStream.range(0, NUMBER_MELONS).forEach(
				$ -> watermelons.add(factory.createRandomMellon()));
		return watermelons;
	}
}
