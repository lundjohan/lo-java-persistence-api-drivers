package se.miun.dsv.jee.jpa.example.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SEEDS")
public class Seed implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;

	@Override
	public String toString() {
		return "Seed[" + hashCode() + "]";
	}
}