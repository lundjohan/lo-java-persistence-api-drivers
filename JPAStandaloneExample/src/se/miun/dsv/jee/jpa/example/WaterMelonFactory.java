package se.miun.dsv.jee.jpa.example;

import java.util.Random;
import java.util.stream.IntStream;

import se.miun.dsv.jee.jpa.example.model.Watermelon;

/**
 * A factory that pumps watermelons out of nowhere
 *
 */
public class WaterMelonFactory {

	private final int maxNumberSeeds;
	private final Random rng;

	public WaterMelonFactory(int maxNumberSeeds) {
		this.maxNumberSeeds = maxNumberSeeds;
		this.rng = new Random(123456789);
	}

	public Watermelon createRandomMellon() {
		return new Watermelon(rng.nextInt(maxNumberSeeds + 1));
	}

	/**
	 * A primitive way to check that the factory works.
	 */
	public static void main(String[] args) {
		WaterMelonFactory factory = new WaterMelonFactory(100);
		IntStream.range(0, 100).forEach(
				$ -> System.out.println(factory.createRandomMellon()));
	}

}