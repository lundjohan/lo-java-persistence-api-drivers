package se.miun.dsv.jee.jpa.example;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import se.miun.dsv.jee.jpa.example.model.Watermelon;

/**
 * A Data Access Object class implementation that hides all database specific code from the caller.
 * 
 */
public class MyWaterMelonDAO {

	private static EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("example");

	public void persistAll(Set<Watermelon> watermelons) {
		EntityManager manager = entityManagerFactory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		try {

			tx.begin();

			for (Watermelon watermelon : watermelons)
				manager.persist(watermelon);

			tx.commit();

		} catch (Exception e) {
			tx.rollback();
			// usually, an application specific exception would be created here
			// and passed on (avoided here for brevity)
			e.printStackTrace();
		} finally {
			manager.close();
		}
	}

	public void shutDown() {
		entityManagerFactory.close();
	}
}
