package se.miun.dsv.jee.jpa.example.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "WATERMELONS")
public class Watermelon implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="WATERMELON")
	private Set<Seed> seeds;

	public Watermelon(int numberSeeds) {
		seeds = initSeeds(numberSeeds);
	}

	private Set<Seed> initSeeds(int numberSeeds) {
		Set<Seed> seeds = new HashSet<>();
		IntStream.range(1, numberSeeds).forEach($ -> seeds.add(new Seed()));
		return Collections.unmodifiableSet(seeds);
	}

	public Set<Seed> getSeeds() {
		return seeds;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Watermelon[");
		builder.append(hashCode());
		builder.append("] has ");
		builder.append(seeds.size());
		builder.append(" seeds: ");
		seeds.stream().forEach(seed -> builder.append(seed + ", "));
		return builder.toString();
	}
}